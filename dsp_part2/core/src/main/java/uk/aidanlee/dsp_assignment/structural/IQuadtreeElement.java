package uk.aidanlee.dsp_assignment.structural;

import com.badlogic.gdx.math.Rectangle;

public interface IQuadtreeElement {
    Rectangle box();
}
