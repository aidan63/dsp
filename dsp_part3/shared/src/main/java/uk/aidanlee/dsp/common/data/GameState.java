package uk.aidanlee.dsp.common.data;

public class GameState {
    // Server State Bytes
    public static final byte LOBBY_ACTIVE    = 0;
    public static final byte LOBBY_COUNTDOWN = 1;

    public static final byte GAME_DEBUG = 2;
}
