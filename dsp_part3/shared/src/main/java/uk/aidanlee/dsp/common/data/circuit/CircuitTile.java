package uk.aidanlee.dsp.common.data.circuit;

import com.badlogic.gdx.math.Vector2;

public class CircuitTile {
    public String id;
    public Vector2[] verts;
    public String frame;
    public String[] tags;
}
