package uk.aidanlee.dsp.common.structural;

import com.badlogic.gdx.math.Rectangle;

public interface IQuadtreeElement {
    Rectangle box();
}
