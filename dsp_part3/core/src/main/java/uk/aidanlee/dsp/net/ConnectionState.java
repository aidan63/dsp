package uk.aidanlee.dsp.net;

public enum ConnectionState {
    Connecting,
    Connected
}
